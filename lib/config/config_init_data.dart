import 'package:app_click_game/model/game_artifact_item.dart';

final List<GameArtifactItem> configArtifacts = [
  GameArtifactItem('LV1', '초급1 항아리', 'common_jar1.jpeg', 14, 3),
  GameArtifactItem('LV2', '초급2 항아리', 'common_jar2.jpeg', 14, 3),
  GameArtifactItem('LV3', '중급1 항아리', 'middle_jar1.jpeg', 14, 3),
  GameArtifactItem('LV4', '중급2 항아리', 'middle_jar2.jpeg', 14, 3),
  GameArtifactItem('LV5', '조선 백자', 'jo_sun_jar.jpeg', 14, 3),
  GameArtifactItem('LV6', '신라 백자', 'back_jar.jpeg', 14, 3),
  GameArtifactItem('LV7', '백제 백자', 'back_jar_1.jpeg', 14, 3),
  GameArtifactItem('LV8', '고급1 항아리', 'jar.jpeg', 14, 3),
  GameArtifactItem('LV9', '고급2 항아리', 'jar1.jpeg', 14, 3),
  GameArtifactItem('LV10', '최고급 항아리', 'expensive_jar.jpg', 14, 3)
];