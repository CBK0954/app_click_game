import 'dart:ui';

import 'package:flutter/material.dart';

class ComponentArtifactItem extends StatelessWidget {
  const ComponentArtifactItem({super.key, required this.imgName, required this.gradeName, required this.artifactName, required this.artifactCount});

  final String imgName;
  final String gradeName;
  final String artifactName;
  final int artifactCount;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          SizedBox(
            width: 80,
            height: 80,
            child: _getImage(),
          ),
          artifactCount == 0 ? Text('-') : _getGradeBadge(),
          Text(artifactCount == 0 ? '-' : artifactName),
          // Text(artifactCount == 0 ? '-' : artifactCount.toString()) // 보유 항아리 개수 카운트
        ],
      ),
    );
  }

  Widget _getGradeBadge() {
    switch(gradeName) {
      case 'LV1':
        return Text(' (하급)  ');
      case 'LV2':
        return Text(' (하급)  ');
      case 'LV3':
        return Text(' (중급)  ');
      case 'LV4':
        return Text(' (중급)  ');
      case 'LV5':
        return Text(' (백자)  ');
      case 'LV6':
        return Text(' (백자)  ');
      case 'LV7':
        return Text(' (백자)  ');
      case 'LV8':
        return Text(' (상급)  ');
      case 'LV9':
        return Text(' (상급)  ');
      default:
        return Text(' (최상급)  ');
    }
  }

  Widget _getImage() {
    if (artifactCount == 0) {
      return ImageFiltered(
        imageFilter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
        child: Image.asset(
          'assets/${imgName}',
          fit: BoxFit.fill,
        ),
      );
    } else {
      return Image.asset(
        'assets/${imgName}',
        fit: BoxFit.fill,
      );
    }
  }
}
