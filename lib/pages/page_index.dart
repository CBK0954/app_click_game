import 'package:app_click_game/config/config_init_data.dart';
import 'package:app_click_game/model/game_artifact_item.dart';
import 'package:app_click_game/pages/page_artifact_list.dart';
import 'package:app_click_game/pages/page_in_game.dart';
import 'package:app_click_game/pages/page_setting.dart';
import 'package:floating_bottom_navigation_bar/floating_bottom_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  int _selectedIndex = 0;

  final List<FloatingNavbarItem> _navItems = [
    FloatingNavbarItem(icon: Icons.directions_run, title: '채집 및 수집'),
    FloatingNavbarItem(icon: Icons.extension, title: '유물 리스트'),
    FloatingNavbarItem(icon: Icons.settings, title: '설정'),
  ];

  final List<Widget> _widgetPages = [
    PageInGame(),
    PageArtifactList(),
    PageSetting(),
  ];

  void _onItemTap(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();

    checkUseArtifacts();
  }

  void checkUseArtifacts() async {
    final prefs = await SharedPreferences.getInstance();
    List<String>? checkData = prefs.getStringList('useArtifacts');

    bool checkIsEmpty = checkData?.isEmpty ?? true;

    if(checkIsEmpty) {
      List<String> result = [];

      for(GameArtifactItem item in configArtifacts) {
        result.add('0');
      }

      prefs.setStringList('useArtifacts', result);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      body: SafeArea(
        child: _widgetPages.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: FloatingNavbar(
        currentIndex: _selectedIndex,
        items: _navItems,
        onTap: _onItemTap,
      ),
    );
  }
}
