import 'dart:math';

import 'package:app_click_game/config/config_init_data.dart';
import 'package:app_click_game/model/choose_artifact_item.dart';
import 'package:app_click_game/model/game_artifact_item.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PageInGame extends StatefulWidget {
  const PageInGame({Key? key}) : super(key: key);

  @override
  State<PageInGame> createState() => _PageInGameState();
}

class _PageInGameState extends State<PageInGame> {
  int _money = 0;
  List<String> _useArtifacts = [];

  @override
  void initState() {
    super.initState();
    _initMoney();
    _getUseArtifacts();
  }

  /// 보유 돈 정보 가져오기. 처음실행이라면 돈은 0원
  void _initMoney() async {
    final prefs = await SharedPreferences.getInstance();
    int oldMoney = prefs.getInt('money') ?? 0;
    setState(() {
      _money = oldMoney;
    });
  }

  /// 돈 정보 저장
  void _saveMoney() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setInt('money', _money);
  }

  /// 클릭시 돈 5000씩 증가
  void _digUpMoney() {
    setState(() {
      _money += 5000;
    });
  }

  /// 보유 유물 정보 가져오기
  void _getUseArtifacts() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      _useArtifacts = prefs.getStringList('useArtifacts') == null ? [] : prefs.getStringList('useArtifacts')!;
    });
  }

  /// 유물 뽑기
  void _getChooseResult(bool isGoldBox) {
    int choosePay = isGoldBox ? 20000 : 5000;
    bool isEnoughMoney = _isStartChooseByMoneyCheck(choosePay);

    if (isEnoughMoney) { // 돈이 충분하면
      setState(() {
        _money -= choosePay;
      });

      int artifactResultId = _getChooseArtifact(isGoldBox); // 랜덤으로 유물 뽑아오기 (index 번호 뽑아옴)

      bool isNewArtifact = false; // 새로 뽑은 유물인지 검사하기 위해 변수 추가하는데 기본으로 아니라고(false) 함.
      int chooseArtifactCount = int.parse(_useArtifacts[artifactResultId]); // 위에서 뽑아온 유물 인덱스 번호에 해당하는 수량이 몇개인지 가져오기.
      if (chooseArtifactCount == 0) isNewArtifact = true; // 만약 기존 보유수량이 0이라면 새로운 유물이라고 알려줌.

      _plusUseArtifact(artifactResultId, chooseArtifactCount); // 보유 유물 수량 1 증가
      _getUseArtifacts(); // 보유 유물 정보 갱신 (원웨이 바인딩이라 새로 알려줘야함.)

      _dialogArtifact(isGoldBox, artifactResultId, isNewArtifact);
    }
  }


  /// 보유 유물 정보 세팅
  void _plusUseArtifact(int index, int oldCount) async {
    List<String> useArtifactTemp = _useArtifacts; // 보유 유물 리스트 복사해옴
    _useArtifacts[index] = (oldCount + 1).toString(); // 수량 증가시킬 유물에 접근해서 수량 1 증가시켜줌

    final prefs = await SharedPreferences.getInstance(); // 메모리 접근
    prefs.setStringList('useArtifacts', useArtifactTemp); // 메모리에 덮어쓰기
  }


  /// 뽑기를 진행함에 있어 돈이 충분한지 확인해주는 메서드
  bool _isStartChooseByMoneyCheck(int choosePay) {
    bool result = false;

    if (_money >= choosePay) result = true;

    return result;
  }

  /// 랜덤으로 유물 하나 뽑아오기
  int _getChooseArtifact(bool isGoldBox) {
    List<GameArtifactItem> artifacts = configArtifacts;

    List<ChooseArtifactItem> percentBar = [];

    double oldPercent = 0;
    int index = 0;
    for (GameArtifactItem artifact in artifacts) {
      ChooseArtifactItem addItem = ChooseArtifactItem(index, oldPercent, isGoldBox ? oldPercent + artifact.goldPercent : oldPercent + artifact.silverPercent);
      percentBar.add(addItem);

      if (isGoldBox) {
        oldPercent += artifact.goldPercent;
      } else {
        oldPercent += artifact.silverPercent;
      }

      index++;
    }

    double percentResult = Random().nextDouble() * 100;

    int resultId = 0;
    for (ChooseArtifactItem item in percentBar) {
      if (percentResult >= item.percentMin && percentResult <= item.percentMax) {
        resultId = item.id;
        break;
      }
    }

    return resultId;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          ElevatedButton(
            onPressed: _digUpMoney,
            child: Text('캐기'),
          ),
          Text('보유한 돈 : ${_money}'),
          ElevatedButton(
            onPressed: () {
              _getChooseResult(false);
            },
            child: Text('은상자 뽑기 (-5000)'),
          ),
          ElevatedButton(
            onPressed: () {
              _getChooseResult(true);
            },
            child: Text('금상자 뽑기 (-20000)'),
          ),
        ],
      ),
    );
  }

  Future<void> _dialogArtifact(bool isGoldBox, int artifactId, bool isNew) async {
    return showDialog<void>(
      //다이얼로그 위젯 소환
      context: context,
      barrierDismissible: false, // 다이얼로그 이외의 바탕 눌러도 안꺼지도록 설정
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(isGoldBox ? '금상자를 뽑았습니다.' : '은상자를 뽑았습니다.'),
          content: SingleChildScrollView(
            child: ListBody(
              //List Body를 기준으로 Text 설정
              children: <Widget>[
                Text('${configArtifacts[artifactId].itemName}을 뽑았습니다.'),
                Text(isNew ? '새로운 항아리!' : '보유 항아리'),
              ],
            ),
          ),
          actions: [
            TextButton(
              child: Text('확인'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  void dispose() {
    _saveMoney();
    super.dispose();
  }
}
