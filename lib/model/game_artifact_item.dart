class GameArtifactItem {
  String rating;
  String itemName;
  String imageName;
  double silverPercent;
  double goldPercent;

  GameArtifactItem(this.rating, this.itemName, this.imageName, this.silverPercent, this.goldPercent);
}